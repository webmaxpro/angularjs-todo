"use strict";

todoApp.factory("tasksService", function() {
    return {
        tasks:
            [
                {name: "Посмотреть фильм", status: true},
                {name: "Убрать дома", status: false},
                {name: "Поработать", status: false}
            ]
    }
});