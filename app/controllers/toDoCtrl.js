"use strict";

    todoApp.controller("toDoCtrl", ['$scope','tasksService', function($scope, tasksService) {
        $scope.tasks = tasksService.tasks;

        $scope.addTask = function() {
            $scope.tasks.push({
                name: $scope.taskName,
                status: false
            });

            $scope.taskName = "";
        };
        $scope.removeTask = function(index) {
          $scope.tasks.splice(index,1);
        };
        $scope.finishTask = function(index) {
            $scope.tasks[index].status = true;
        };

        $scope.setStatus = function(status) {
            return status ? 'Да' : 'Нет';
        };
        $scope.setStatusColor = function(status) {
            return status ? 'statusTrue' : 'statusFalse';
        };
        $scope.setStatusName = function (status) {
            return status ? 'statusTrueName' : '';
        };
    }]);